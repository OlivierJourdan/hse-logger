# hse-logger


## :memo: Description 

A OOP based logging system inspired by the Phython logging module.


## :rocket: Installation

The latest release version can be found at 
https://dokuwiki.hampel-soft.com/code/hse-libraries/hse-logger


## :bulb: Usage

tbd.


## :wrench: Configuration 

No configuration needed.


## :busts_in_silhouette: Contributing 

Please contribute! We'd love to see code coming back to this repo. Get in touch if you want to know more about contributing.

##  :beers: Credits

* Joerg Hampel
* Manuel Sebald

## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details